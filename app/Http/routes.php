<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*Route de imagem*/
Route::get('profile', 'UserController@profile');
Route::post('profile', 'UserController@update_avatar');


/*Route de update de senha*/
Route::get('user_update/password', 'UserController@password');
Route::post('user/updatepassword', 'UserController@updatePassword');




Route::auth();
/*Route da pagina principal*/
Route::get('/home', 'HomeController@index');
