<?php

namespace Condominio\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Condominio\Http\Requests;
use Auth;
use Image;
use Hash;
use Condominio\Models\User;


class UserController extends Controller
{
    //
    public function profile(){
    	return view('profile', array('user' => Auth::user()) );
    }

    public function update_avatar(Request $request){

    	// Handle the user upload of avatar
    	if($request->hasFile('avatar')){
    		$avatar = $request->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );

    		$user = Auth::user();
    		$user->avatar = $filename;
    		$user->save();
    	}

    	return view('profile', array('user' => Auth::user()) );

    }

    public function password(){
       return View('auth.user_update.password');
    }


    public function updatePassword(Request $request){

        $rules = [
            'mypassword'            => 'required',
                //A senha deve conter letras maiúscula, minúscula numeros e caractere especial e força de 8 a 32
                    'password'      => [
                    'required',
                    'confirmed',
                    'min:8',
                    'max:32',
                    'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^A-Z^a-z^\d^\s]).{8,32}/',
                ]

             ];

        $messages = [
            'mypassword.required'    => 'Campo obrigatorio',
            'password.required'      => 'Campo obrigatorio',
            'password.confirmed'     => 'As senhas de redefinição não são iguais',
            'password.min'           => 'A senha deve possuir no mínimo 8 caracteres.',
            'password.max'           => 'A senha deve possuir no maxímo 32 caracteres.',
             ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()){
            return redirect('user_update/password')->withErrors($validator);
        }
        else{
            if (Hash::check($request->mypassword, Auth::user()->password)){
                $user = new User;
                $user->where('email', '=', Auth::user()->email)
                     ->update(['password' => bcrypt($request->password)]);

                return redirect('user_update/password')->with('status', 'Senha alterada com sucesso');
            }
            else
            {
                return redirect('user_update/password')->with('message', 'O Senha atual é inválido.');
            }
        }
    }

}
